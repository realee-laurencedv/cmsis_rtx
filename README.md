# CMSIS RTX 5

We are not the author of this code, this repo exist only to provide git access to a small portion of code (without all the cmsis components), namely: RTX 5 RTOS source supplied by [ARM](https://developer.arm.com/tools-and-software/embedded/cmsis)  
See [original repo](https://github.com/ARM-software/CMSIS_5) for more info  
Contact [maintainer][maintainer_email] for information and/or questions


## Deployment

1. Add to **ignore** in all tools the _example_ folder
2. Add to **include** path of compiler/assembler/etc the _inc_ folder
3. Add to **compile** path of compiler the _src_ folder
4. **Selectively ignore** the unnecessary _irq_ src file (ie: for Cortex-M4 only irq_cm4.S should be enabled)
5. Ensure all **dep are present** in the parent project

    - [CMSIS Core][cmsiscorerepo_link]
    - [CMSIS RTOS2][cmsisrtos2repo_link]

6. Look into the example folder for how to implement minimal structure in your own project to make RTX happy, but mainly you must:

    - Have a file named `RTE_Components.h` with a unspecific content (for now)
    - If using MDK's event recorder, have a `EventRecorderConf.h` file with specific defines, check [example](example/EventRecorderConf.h) or the [official doc][mdkeventrecorder_link]
    - Make a copy of the provided [RTX_Config.c](example/RTX_Config.c) and [RTX_Config.h](example/RTX_Config.c) in you project
        - Now is a good time to look into those files and see the availables options, adjust them if needed
    - *Optional:* Copy the file [handlers.c](example/handlers.c) or its content in your code base to have an easy hook on generic cpu exception (may collide with vendor specific ones)
    - **Reminder**: If you are using **CM0+** target and GCC as compiler, you will get error about register *r7*. One solution is to forbid gcc to use r7 as frame-pointer by adding this compilation option: `-fomit-frame-pointer`


[maintainer_email]:     	<MAILTO:laurencedv@realee.tech>
[cmsiscorerepo_link]:   	https://gitlab.com/real-ee/cmsis/cmsis_core
[cmsisrtos2repo_link]:  	https://gitlab.com/real-ee/cmsis/cmsis_rtos2
[mdkeventrecorder_link]:	https://www.keil.com/pack/doc/compiler/EventRecorder/html/index.html

