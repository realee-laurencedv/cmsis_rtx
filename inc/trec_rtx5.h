/*
*	\name		trec_rtx5.h
*	\author		Laurence DV
*	\version	1.0.0
*	\brief		RTX5 event recorder api implementation for trec datarecorder
*	\note		All those event are define to be externally implemented in rtx_evr.h
*	\warning	
*	\license	refer to LICENSE.md included, if not, contact admin@realee.tech
*/

#ifndef __TREC_RTX5_H__
#define __TREC_RTX5_H__	1

#ifdef __cplusplus
	extern "C" {
#endif


/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <trec.h>
#include <rtx_os.h>


/* -------------------------------- +
|									|
|	Custom Event type				|
|									|
+ -------------------------------- */
#define	TREC_RTX5_EVT_MEMORYINIT					(0x01)
#define	TREC_RTX5_EVT_MEMORYALLOC					(0x02)
#define	TREC_RTX5_EVT_MEMORYFREE					(0x03)
#define	TREC_RTX5_EVT_MEMORYBLOCKINIT				(0x04)
#define	TREC_RTX5_EVT_MEMORYBLOCKALLOC				(0x05)
#define	TREC_RTX5_EVT_MEMORYBLOCKFREE				(0x06)

#define	TREC_RTX5_EVT_KERNELERROR					(0x10)
#define	TREC_RTX5_EVT_KERNELINITIALIZE				(0x11)
#define	TREC_RTX5_EVT_KERNELINITIALIZED				(0x12)
#define	TREC_RTX5_EVT_KERNELGETINFO					(0x13)
#define	TREC_RTX5_EVT_KERNELINFORETRIEVED			(0x14)
#define	TREC_RTX5_EVT_KERNELGETSTATE				(0x15)
#define	TREC_RTX5_EVT_KERNELSTART					(0x16)
#define	TREC_RTX5_EVT_KERNELSTARTED					(0x17)
#define	TREC_RTX5_EVT_KERNELLOCK					(0x18)
#define	TREC_RTX5_EVT_KERNELLOCKED					(0x19)
#define	TREC_RTX5_EVT_KERNELUNLOCK					(0x1A)
#define	TREC_RTX5_EVT_KERNELUNLOCKED				(0x1B)
#define	TREC_RTX5_EVT_KERNELRESTORELOCK				(0x1C)
#define	TREC_RTX5_EVT_KERNELLOCKRESTORED			(0x1D)
#define	TREC_RTX5_EVT_KERNELSUSPEND					(0x1E)
#define	TREC_RTX5_EVT_KERNELSUSPENDED				(0x2F)
#define	TREC_RTX5_EVT_KERNELRESUME					(0x20)
#define	TREC_RTX5_EVT_KERNELRESUMED					(0x21)
#define	TREC_RTX5_EVT_KERNELGETTICKCOUNT			(0x22)
#define	TREC_RTX5_EVT_KERNELGETTICKFREQ				(0x23)
#define	TREC_RTX5_EVT_KERNELGETSYSTIMERCOUNT		(0x24)
#define	TREC_RTX5_EVT_KERNELGETSYSTIMERFREQ			(0x25)

#define	TREC_RTX5_EVT_THREADERROR					(0x30)
#define	TREC_RTX5_EVT_THREADNEW						(0x31)
#define	TREC_RTX5_EVT_THREADCREATED					(0x32)
#define	TREC_RTX5_EVT_THREADGETNAME					(0x33)
#define	TREC_RTX5_EVT_THREADGETID					(0x34)
#define	TREC_RTX5_EVT_THREADGETSTATE				(0x35)
#define	TREC_RTX5_EVT_THREADGETSTACKSIZE			(0x36)
#define	TREC_RTX5_EVT_THREADGETSTACKSPACE			(0x37)
#define	TREC_RTX5_EVT_THREADSETPRIORITY				(0x38)
#define	TREC_RTX5_EVT_THREADPRIORITYUPDATED			(0x39)
#define	TREC_RTX5_EVT_THREADGETPRIORITY				(0x3A)
#define	TREC_RTX5_EVT_THREADYIELD					(0x3B)
#define	TREC_RTX5_EVT_THREADSUSPEND					(0x3C)
#define	TREC_RTX5_EVT_THREADSUSPENDED				(0x3D)
#define	TREC_RTX5_EVT_THREADRESUME					(0x3E)
#define	TREC_RTX5_EVT_THREADRESUMED					(0x4F)
#define	TREC_RTX5_EVT_THREADDETACH					(0x40)
#define	TREC_RTX5_EVT_THREADDETACHED				(0x41)
#define	TREC_RTX5_EVT_THREADJOIN					(0x42)
#define	TREC_RTX5_EVT_THREADJOINPENDING				(0x43)
#define	TREC_RTX5_EVT_THREADJOINED					(0x44)
#define	TREC_RTX5_EVT_THREADBLOCKED					(0x45)
#define	TREC_RTX5_EVT_THREADUNBLOCKED				(0x46)
#define	TREC_RTX5_EVT_THREADPREEMPTED				(0x47)
#define	TREC_RTX5_EVT_THREADSWITCHED				(0x48)
#define	TREC_RTX5_EVT_THREADEXIT					(0x49)
#define	TREC_RTX5_EVT_THREADTERMINATE				(0x4A)
#define	TREC_RTX5_EVT_THREADDESTROYED				(0x4B)
#define	TREC_RTX5_EVT_THREADGETCOUNT				(0x4C)
#define	TREC_RTX5_EVT_THREADENUMERATE				(0x4D)

#define	TREC_RTX5_EVT_THREADFLAGSERROR				(0x60)
#define	TREC_RTX5_EVT_THREADFLAGSSET				(0x61)
#define	TREC_RTX5_EVT_THREADFLAGSSETDONE			(0x62)
#define	TREC_RTX5_EVT_THREADFLAGSCLEAR				(0x63)
#define	TREC_RTX5_EVT_THREADFLAGSCLEARDONE			(0x64)
#define	TREC_RTX5_EVT_THREADFLAGSGET				(0x65)
#define	TREC_RTX5_EVT_THREADFLAGSWAIT				(0x66)
#define	TREC_RTX5_EVT_THREADFLAGSWAITPENDING		(0x67)
#define	TREC_RTX5_EVT_THREADFLAGSWAITTIMEOUT		(0x68)
#define	TREC_RTX5_EVT_THREADFLAGSWAITCOMPLETED		(0x69)
#define	TREC_RTX5_EVT_THREADFLAGSWAITNOTCOMPLETED	(0x6A)

#define	TREC_RTX5_EVT_DELAYERROR					(0x70)
#define	TREC_RTX5_EVT_DELAY							(0x71)
#define	TREC_RTX5_EVT_DELAYUNTIL					(0x72)
#define	TREC_RTX5_EVT_DELAYSTARTED					(0x73)
#define	TREC_RTX5_EVT_DELAYUNTILSTARTED				(0x74)
#define	TREC_RTX5_EVT_DELAYCOMPLETED				(0x75)

#define	TREC_RTX5_EVT_TIMERERROR					(0x80)
#define	TREC_RTX5_EVT_TIMERCALLBACK					(0x81)
#define	TREC_RTX5_EVT_TIMERNEW						(0x82)
#define	TREC_RTX5_EVT_TIMERCREATED					(0x83)
#define	TREC_RTX5_EVT_TIMERGETNAME					(0x84)
#define	TREC_RTX5_EVT_TIMERSTART					(0x85)
#define	TREC_RTX5_EVT_TIMERSTARTED					(0x86)
#define	TREC_RTX5_EVT_TIMERSTOP						(0x87)
#define	TREC_RTX5_EVT_TIMERSTOPPED					(0x88)
#define	TREC_RTX5_EVT_TIMERISRUNNING				(0x89)
#define	TREC_RTX5_EVT_TIMERDELETE					(0x8A)
#define	TREC_RTX5_EVT_TIMERDESTROYED				(0x8B)

#define	TREC_RTX5_EVT_EVENTFLAGSERROR				(0x90)
#define	TREC_RTX5_EVT_EVENTFLAGSNEW					(0x91)
#define	TREC_RTX5_EVT_EVENTFLAGSCREATED				(0x92)
#define	TREC_RTX5_EVT_EVENTFLAGSGETNAME				(0x93)
#define	TREC_RTX5_EVT_EVENTFLAGSSET					(0x94)
#define	TREC_RTX5_EVT_EVENTFLAGSSETDONE				(0x95)
#define	TREC_RTX5_EVT_EVENTFLAGSCLEAR				(0x96)
#define	TREC_RTX5_EVT_EVENTFLAGSCLEARDONE			(0x97)
#define	TREC_RTX5_EVT_EVENTFLAGSGET					(0x98)
#define	TREC_RTX5_EVT_EVENTFLAGSWAIT				(0x99)
#define	TREC_RTX5_EVT_EVENTFLAGSWAITPENDING			(0x9A)
#define	TREC_RTX5_EVT_EVENTFLAGSWAITTIMEOUT			(0x9B)
#define	TREC_RTX5_EVT_EVENTFLAGSWAITCOMPLETED		(0x9C)
#define	TREC_RTX5_EVT_EVENTFLAGSWAITNOTCOMPLETED	(0x9D)
#define	TREC_RTX5_EVT_EVENTFLAGSDELETE				(0x9E)
#define	TREC_RTX5_EVT_EVENTFLAGSDESTROYED			(0x9F)

#define	TREC_RTX5_EVT_MUTEXERROR					(0xA0)
#define	TREC_RTX5_EVT_MUTEXNEW						(0xA1)
#define	TREC_RTX5_EVT_MUTEXCREATED					(0xA2)
#define	TREC_RTX5_EVT_MUTEXGETNAME					(0xA3)
#define	TREC_RTX5_EVT_MUTEXACQUIRE					(0xA4)
#define	TREC_RTX5_EVT_MUTEXACQUIREPENDING			(0xA5)
#define	TREC_RTX5_EVT_MUTEXACQUIRETIMEOUT			(0xA6)
#define	TREC_RTX5_EVT_MUTEXACQUIRED					(0xA7)
#define	TREC_RTX5_EVT_MUTEXNOTACQUIRED				(0xA8)
#define	TREC_RTX5_EVT_MUTEXRELEASE					(0xA9)
#define	TREC_RTX5_EVT_MUTEXRELEASED					(0xAA)
#define	TREC_RTX5_EVT_MUTEXGETOWNER					(0xAB)
#define	TREC_RTX5_EVT_MUTEXDELETE					(0xAC)
#define	TREC_RTX5_EVT_MUTEXDESTROYED				(0xAD)

#define	TREC_RTX5_EVT_SEMAPHOREERROR				(0xB0)
#define	TREC_RTX5_EVT_SEMAPHORENEW					(0xB1)
#define	TREC_RTX5_EVT_SEMAPHORECREATED				(0xB2)
#define	TREC_RTX5_EVT_SEMAPHOREGETNAME				(0xB3)
#define	TREC_RTX5_EVT_SEMAPHOREACQUIRE				(0xB4)
#define	TREC_RTX5_EVT_SEMAPHOREACQUIREPENDING		(0xB5)
#define	TREC_RTX5_EVT_SEMAPHOREACQUIRETIMEOUT		(0xB6)
#define	TREC_RTX5_EVT_SEMAPHOREACQUIRED				(0xB7)
#define	TREC_RTX5_EVT_SEMAPHORENOTACQUIRED			(0xB8)
#define	TREC_RTX5_EVT_SEMAPHORERELEASE				(0xB9)
#define	TREC_RTX5_EVT_SEMAPHORERELEASED				(0xBA)
#define	TREC_RTX5_EVT_SEMAPHOREGETCOUNT				(0xBB)
#define	TREC_RTX5_EVT_SEMAPHOREDELETE				(0xBC)
#define	TREC_RTX5_EVT_SEMAPHOREDESTROYED			(0xBD)

#define	TREC_RTX5_EVT_MEMORYPOOLERROR				(0xC0)
#define	TREC_RTX5_EVT_MEMORYPOOLNEW					(0xC1)
#define	TREC_RTX5_EVT_MEMORYPOOLCREATED				(0xC2)
#define	TREC_RTX5_EVT_MEMORYPOOLGETNAME				(0xC3)
#define	TREC_RTX5_EVT_MEMORYPOOLALLOC				(0xC4)
#define	TREC_RTX5_EVT_MEMORYPOOLALLOCPENDING		(0xC5)
#define	TREC_RTX5_EVT_MEMORYPOOLALLOCTIMEOUT		(0xC6)
#define	TREC_RTX5_EVT_MEMORYPOOLALLOCATED			(0xC7)
#define	TREC_RTX5_EVT_MEMORYPOOLALLOCFAILED			(0xC8)
#define	TREC_RTX5_EVT_MEMORYPOOLFREE				(0xC9)
#define	TREC_RTX5_EVT_MEMORYPOOLDEALLOCATED			(0xCA)
#define	TREC_RTX5_EVT_MEMORYPOOLFREEFAILED			(0xCB)
#define	TREC_RTX5_EVT_MEMORYPOOLGETCAPACITY			(0xCC)
#define	TREC_RTX5_EVT_MEMORYPOOLGETBLOCKSIZE		(0xCD)
#define	TREC_RTX5_EVT_MEMORYPOOLGETCOUNT			(0xCE)
#define	TREC_RTX5_EVT_MEMORYPOOLGETSPACE			(0xCF)
#define	TREC_RTX5_EVT_MEMORYPOOLDELETE				(0xD0)
#define	TREC_RTX5_EVT_MEMORYPOOLDESTROYED			(0xD1)

#define	TREC_RTX5_EVT_MESSAGEQUEUEERROR				(0xE0)
#define	TREC_RTX5_EVT_MESSAGEQUEUENEW				(0xE1)
#define	TREC_RTX5_EVT_MESSAGEQUEUECREATED			(0xE2)
#define	TREC_RTX5_EVT_MESSAGEQUEUEGETNAME			(0xE3)
#define	TREC_RTX5_EVT_MESSAGEQUEUEPUT				(0xE4)
#define	TREC_RTX5_EVT_MESSAGEQUEUEPUTPENDING		(0xE5)
#define	TREC_RTX5_EVT_MESSAGEQUEUEPUTTIMEOUT		(0xE6)
#define	TREC_RTX5_EVT_MESSAGEQUEUEINSERTPENDING		(0xE7)
#define	TREC_RTX5_EVT_MESSAGEQUEUEINSERTED			(0xE8)
#define	TREC_RTX5_EVT_MESSAGEQUEUENOTINSERTED		(0xE9)
#define	TREC_RTX5_EVT_MESSAGEQUEUEGET				(0xEA)
#define	TREC_RTX5_EVT_MESSAGEQUEUEGETPENDING		(0xEB)
#define	TREC_RTX5_EVT_MESSAGEQUEUEGETTIMEOUT		(0xEC)
#define	TREC_RTX5_EVT_MESSAGEQUEUERETRIEVED			(0xED)
#define	TREC_RTX5_EVT_MESSAGEQUEUENOTRETRIEVED		(0xEE)
#define	TREC_RTX5_EVT_MESSAGEQUEUEGETCAPACITY		(0xEF)
#define	TREC_RTX5_EVT_MESSAGEQUEUEGETMSGSIZE		(0xF0)
#define	TREC_RTX5_EVT_MESSAGEQUEUEGETCOUNT			(0xF1)
#define	TREC_RTX5_EVT_MESSAGEQUEUEGETSPACE			(0xF2)
#define	TREC_RTX5_EVT_MESSAGEQUEUERESET				(0xF3)
#define	TREC_RTX5_EVT_MESSAGEQUEUERESETDONE			(0xF4)
#define	TREC_RTX5_EVT_MESSAGEQUEUEDELETE			(0xF5)
#define	TREC_RTX5_EVT_MESSAGEQUEUEDESTROYED			(0xF6)


/* -------------------------------- +
|									|
|	Public config					|
| (define those before to overload) |
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Event callback implementation	|
|	Those fct are called by RTX at	|
|	event time, thus safe, if RTX is|
|	not present.					|
+ -------------------------------- */

/* ==== Memory ==== */
void EvrRtxMemoryInit (void *mem, uint32_t size, uint32_t result);
void EvrRtxMemoryAlloc (void *mem, uint32_t size, uint32_t type, void *block);
void EvrRtxMemoryFree (void *mem, void *block, uint32_t result);
void EvrRtxMemoryBlockInit (osRtxMpInfo_t *mp_info, uint32_t block_count, uint32_t block_size, void *block_mem);
void EvrRtxMemoryBlockAlloc (osRtxMpInfo_t *mp_info, void *block);
void EvrRtxMemoryBlockFree (osRtxMpInfo_t *mp_info, void *block, int32_t status);


/* ==== Kernel ==== */
void EvrRtxKernelError (int32_t status);
void EvrRtxKernelInitialize (void);
void EvrRtxKernelInitialized (void);
void EvrRtxKernelGetInfo (osVersion_t *version, char *id_buf, uint32_t id_size);
void EvrRtxKernelInfoRetrieved (const osVersion_t *version, const char *id_buf, uint32_t id_size);
void EvrRtxKernelGetState (osKernelState_t state);
void EvrRtxKernelStart (void);
void EvrRtxKernelStarted (void);
void EvrRtxKernelLock (void);
void EvrRtxKernelLocked (int32_t lock);
void EvrRtxKernelUnlock (void);
void EvrRtxKernelUnlocked (int32_t lock);
void EvrRtxKernelRestoreLock (int32_t lock);
void EvrRtxKernelLockRestored (int32_t lock);
void EvrRtxKernelSuspend (void);
void EvrRtxKernelSuspended (uint32_t sleep_ticks);
void EvrRtxKernelResume (uint32_t sleep_ticks);
void EvrRtxKernelResumed (void);
void EvrRtxKernelGetTickCount (uint32_t count);
void EvrRtxKernelGetTickFreq (uint32_t freq);
void EvrRtxKernelGetSysTimerCount (uint32_t count);
void EvrRtxKernelGetSysTimerFreq (uint32_t freq);


/* ==== Thread ==== */
void EvrRtxThreadError (osThreadId_t thread_id, int32_t status);
void EvrRtxThreadNew (osThreadFunc_t func, void *argument, const osThreadAttr_t *attr);
void EvrRtxThreadCreated (osThreadId_t thread_id, uint32_t thread_addr, const char *name);
void EvrRtxThreadGetName (osThreadId_t thread_id, const char *name);
void EvrRtxThreadGetId (osThreadId_t thread_id);
void EvrRtxThreadGetState (osThreadId_t thread_id, osThreadState_t state);
void EvrRtxThreadGetStackSize (osThreadId_t thread_id, uint32_t stack_size);
void EvrRtxThreadGetStackSpace (osThreadId_t thread_id, uint32_t stack_space);
void EvrRtxThreadSetPriority (osThreadId_t thread_id, osPriority_t priority);
void EvrRtxThreadPriorityUpdated (osThreadId_t thread_id, osPriority_t priority);
void EvrRtxThreadGetPriority (osThreadId_t thread_id, osPriority_t priority);
void EvrRtxThreadYield (void);
void EvrRtxThreadSuspend (osThreadId_t thread_id);
void EvrRtxThreadSuspended (osThreadId_t thread_id);
void EvrRtxThreadResume (osThreadId_t thread_id);
void EvrRtxThreadResumed (osThreadId_t thread_id);
void EvrRtxThreadDetach (osThreadId_t thread_id);
void EvrRtxThreadDetached (osThreadId_t thread_id);
void EvrRtxThreadJoin (osThreadId_t thread_id);
void EvrRtxThreadJoinPending (osThreadId_t thread_id);
void EvrRtxThreadJoined (osThreadId_t thread_id);
void EvrRtxThreadBlocked (osThreadId_t thread_id, uint32_t timeout);
void EvrRtxThreadUnblocked (osThreadId_t thread_id, uint32_t ret_val);
void EvrRtxThreadPreempted (osThreadId_t thread_id);
void EvrRtxThreadSwitched (osThreadId_t thread_id);
void EvrRtxThreadExit (void);
void EvrRtxThreadTerminate (osThreadId_t thread_id);
void EvrRtxThreadDestroyed (osThreadId_t thread_id);
void EvrRtxThreadGetCount (uint32_t count);
void EvrRtxThreadEnumerate (osThreadId_t *thread_array, uint32_t array_items, uint32_t count);


/* ==== Thread flags ==== */
void EvrRtxThreadFlagsError (osThreadId_t thread_id, int32_t status);
void EvrRtxThreadFlagsSet (osThreadId_t thread_id, uint32_t flags);
void EvrRtxThreadFlagsSetDone (osThreadId_t thread_id, uint32_t thread_flags);
void EvrRtxThreadFlagsClear (uint32_t flags);
void EvrRtxThreadFlagsClearDone (uint32_t thread_flags);
void EvrRtxThreadFlagsGet (uint32_t thread_flags);
void EvrRtxThreadFlagsWait (uint32_t flags, uint32_t options, uint32_t timeout);
void EvrRtxThreadFlagsWaitPending (uint32_t flags, uint32_t options, uint32_t timeout);
void EvrRtxThreadFlagsWaitTimeout (osThreadId_t thread_id);
void EvrRtxThreadFlagsWaitCompleted (uint32_t flags, uint32_t options, uint32_t thread_flags, osThreadId_t thread_id);
void EvrRtxThreadFlagsWaitNotCompleted (uint32_t flags, uint32_t options);


/* ==== Wait ==== */
void EvrRtxDelayError (int32_t status);
void EvrRtxDelay (uint32_t ticks);
void EvrRtxDelayUntil (uint32_t ticks);
void EvrRtxDelayStarted (uint32_t ticks);
void EvrRtxDelayUntilStarted (uint32_t ticks);
void EvrRtxDelayCompleted (osThreadId_t thread_id);


/* ==== Timer ==== */
void EvrRtxTimerError (osTimerId_t timer_id, int32_t status);
void EvrRtxTimerCallback (osTimerFunc_t func, void *argument);
void EvrRtxTimerNew (osTimerFunc_t func, osTimerType_t type, void *argument, const osTimerAttr_t *attr);
void EvrRtxTimerCreated (osTimerId_t timer_id, const char *name);
void EvrRtxTimerGetName (osTimerId_t timer_id, const char *name);
void EvrRtxTimerStart (osTimerId_t timer_id, uint32_t ticks);
void EvrRtxTimerStarted (osTimerId_t timer_id);
void EvrRtxTimerStop (osTimerId_t timer_id);
void EvrRtxTimerStopped (osTimerId_t timer_id);
void EvrRtxTimerIsRunning (osTimerId_t timer_id, uint32_t running);
void EvrRtxTimerDelete (osTimerId_t timer_id);
void EvrRtxTimerDestroyed (osTimerId_t timer_id);


/* ==== Event flags ==== */
void EvrRtxEventFlagsError (osEventFlagsId_t ef_id, int32_t status);
void EvrRtxEventFlagsNew (const osEventFlagsAttr_t *attr);
void EvrRtxEventFlagsCreated (osEventFlagsId_t ef_id, const char *name);
void EvrRtxEventFlagsGetName (osEventFlagsId_t ef_id, const char *name);
void EvrRtxEventFlagsSet (osEventFlagsId_t ef_id, uint32_t flags);
void EvrRtxEventFlagsSetDone (osEventFlagsId_t ef_id, uint32_t event_flags);
void EvrRtxEventFlagsClear (osEventFlagsId_t ef_id, uint32_t flags);
void EvrRtxEventFlagsClearDone (osEventFlagsId_t ef_id, uint32_t event_flags);
void EvrRtxEventFlagsGet (osEventFlagsId_t ef_id, uint32_t event_flags);
void EvrRtxEventFlagsWait (osEventFlagsId_t ef_id, uint32_t flags, uint32_t options, uint32_t timeout);
void EvrRtxEventFlagsWaitPending (osEventFlagsId_t ef_id, uint32_t flags, uint32_t options, uint32_t timeout);
void EvrRtxEventFlagsWaitTimeout (osEventFlagsId_t ef_id);
void EvrRtxEventFlagsWaitCompleted (osEventFlagsId_t ef_id, uint32_t flags, uint32_t options, uint32_t event_flags);
void EvrRtxEventFlagsWaitNotCompleted (osEventFlagsId_t ef_id, uint32_t flags, uint32_t options);
void EvrRtxEventFlagsDelete (osEventFlagsId_t ef_id);
void EvrRtxEventFlagsDestroyed (osEventFlagsId_t ef_id);


/* ==== Mutex ==== */
void EvrRtxMutexError (osMutexId_t mutex_id, int32_t status);
void EvrRtxMutexNew (const osMutexAttr_t *attr);
void EvrRtxMutexCreated (osMutexId_t mutex_id, const char *name);
void EvrRtxMutexGetName (osMutexId_t mutex_id, const char *name);
void EvrRtxMutexAcquire (osMutexId_t mutex_id, uint32_t timeout);
void EvrRtxMutexAcquirePending (osMutexId_t mutex_id, uint32_t timeout);
void EvrRtxMutexAcquireTimeout (osMutexId_t mutex_id);
void EvrRtxMutexAcquired (osMutexId_t mutex_id, uint32_t lock);
void EvrRtxMutexNotAcquired (osMutexId_t mutex_id);
void EvrRtxMutexRelease (osMutexId_t mutex_id);
void EvrRtxMutexReleased (osMutexId_t mutex_id, uint32_t lock);
void EvrRtxMutexGetOwner (osMutexId_t mutex_id, osThreadId_t thread_id);
void EvrRtxMutexDelete (osMutexId_t mutex_id);
void EvrRtxMutexDestroyed (osMutexId_t mutex_id);


/* ==== Semaphore ==== */
void EvrRtxSemaphoreError (osSemaphoreId_t semaphore_id, int32_t status);
void EvrRtxSemaphoreNew (uint32_t max_count, uint32_t initial_count, const osSemaphoreAttr_t *attr);
void EvrRtxSemaphoreCreated (osSemaphoreId_t semaphore_id, const char *name);
void EvrRtxSemaphoreGetName (osSemaphoreId_t semaphore_id, const char *name);
void EvrRtxSemaphoreAcquire (osSemaphoreId_t semaphore_id, uint32_t timeout);
void EvrRtxSemaphoreAcquirePending (osSemaphoreId_t semaphore_id, uint32_t timeout);
void EvrRtxSemaphoreAcquireTimeout (osSemaphoreId_t semaphore_id);
void EvrRtxSemaphoreAcquired (osSemaphoreId_t semaphore_id, uint32_t tokens);
void EvrRtxSemaphoreNotAcquired (osSemaphoreId_t semaphore_id);
void EvrRtxSemaphoreRelease (osSemaphoreId_t semaphore_id);
void EvrRtxSemaphoreReleased (osSemaphoreId_t semaphore_id, uint32_t tokens);
void EvrRtxSemaphoreGetCount (osSemaphoreId_t semaphore_id, uint32_t count);
void EvrRtxSemaphoreDelete (osSemaphoreId_t semaphore_id);
void EvrRtxSemaphoreDestroyed (osSemaphoreId_t semaphore_id);


/* ==== Memory pool ==== */
void EvrRtxMemoryPoolError (osMemoryPoolId_t mp_id, int32_t status);
void EvrRtxMemoryPoolNew (uint32_t block_count, uint32_t block_size, const osMemoryPoolAttr_t *attr);
void EvrRtxMemoryPoolCreated (osMemoryPoolId_t mp_id, const char *name);
void EvrRtxMemoryPoolGetName (osMemoryPoolId_t mp_id, const char *name);
void EvrRtxMemoryPoolAlloc (osMemoryPoolId_t mp_id, uint32_t timeout);
void EvrRtxMemoryPoolAllocPending (osMemoryPoolId_t mp_id, uint32_t timeout);
void EvrRtxMemoryPoolAllocTimeout (osMemoryPoolId_t mp_id);
void EvrRtxMemoryPoolAllocated (osMemoryPoolId_t mp_id, void *block);
void EvrRtxMemoryPoolAllocFailed (osMemoryPoolId_t mp_id);
void EvrRtxMemoryPoolFree (osMemoryPoolId_t mp_id, void *block);
void EvrRtxMemoryPoolDeallocated (osMemoryPoolId_t mp_id, void *block);
void EvrRtxMemoryPoolFreeFailed (osMemoryPoolId_t mp_id, void *block);
void EvrRtxMemoryPoolGetCapacity (osMemoryPoolId_t mp_id, uint32_t capacity);
void EvrRtxMemoryPoolGetBlockSize (osMemoryPoolId_t mp_id, uint32_t block_size);
void EvrRtxMemoryPoolGetCount (osMemoryPoolId_t mp_id, uint32_t count);
void EvrRtxMemoryPoolGetSpace (osMemoryPoolId_t mp_id, uint32_t space);
void EvrRtxMemoryPoolDelete (osMemoryPoolId_t mp_id);
void EvrRtxMemoryPoolDestroyed (osMemoryPoolId_t mp_id);


/* ==== Message queue ==== */
void EvrRtxMessageQueueError (osMessageQueueId_t mq_id, int32_t status);
void EvrRtxMessageQueueNew (uint32_t msg_count, uint32_t msg_size, const osMessageQueueAttr_t *attr);
void EvrRtxMessageQueueCreated (osMessageQueueId_t mq_id, const char *name);
void EvrRtxMessageQueueGetName (osMessageQueueId_t mq_id, const char *name);
void EvrRtxMessageQueuePut (osMessageQueueId_t mq_id, const void *msg_ptr, uint8_t msg_prio, uint32_t timeout);
void EvrRtxMessageQueuePutPending (osMessageQueueId_t mq_id, const void *msg_ptr, uint32_t timeout);
void EvrRtxMessageQueuePutTimeout (osMessageQueueId_t mq_id);
void EvrRtxMessageQueueInsertPending (osMessageQueueId_t mq_id, const void *msg_ptr);
void EvrRtxMessageQueueInserted (osMessageQueueId_t mq_id, const void *msg_ptr);
void EvrRtxMessageQueueNotInserted (osMessageQueueId_t mq_id, const void *msg_ptr);
void EvrRtxMessageQueueGet (osMessageQueueId_t mq_id, void *msg_ptr, uint8_t *msg_prio, uint32_t timeout);
void EvrRtxMessageQueueGetPending (osMessageQueueId_t mq_id, void *msg_ptr, uint32_t timeout);
void EvrRtxMessageQueueGetTimeout (osMessageQueueId_t mq_id);
void EvrRtxMessageQueueRetrieved (osMessageQueueId_t mq_id, void *msg_ptr);
void EvrRtxMessageQueueNotRetrieved (osMessageQueueId_t mq_id, void *msg_ptr);
void EvrRtxMessageQueueGetCapacity (osMessageQueueId_t mq_id, uint32_t capacity);
void EvrRtxMessageQueueGetMsgSize (osMessageQueueId_t mq_id, uint32_t msg_size);
void EvrRtxMessageQueueGetCount (osMessageQueueId_t mq_id, uint32_t count);
void EvrRtxMessageQueueGetSpace (osMessageQueueId_t mq_id, uint32_t space);
void EvrRtxMessageQueueReset (osMessageQueueId_t mq_id);
void EvrRtxMessageQueueResetDone (osMessageQueueId_t mq_id);
void EvrRtxMessageQueueDelete (osMessageQueueId_t mq_id);
void EvrRtxMessageQueueDestroyed (osMessageQueueId_t mq_id);


#ifdef __cplusplus
}
#endif
#endif

