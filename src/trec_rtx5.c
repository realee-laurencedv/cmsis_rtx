
/*
	Check header for details
*/
#include <trec_rtx5.h>

#pragma GCC diagnostic ignored "-Wunused-function"		/* Disable the warning from unused private function in your lib */
#pragma GCC diagnostic ignored "-Wunused-parameter"
/* -------------------------------- +
|									|
|	Private Constant				|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Global Variable					|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Private Macro					|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Event callback implementation	|
|									|
+ -------------------------------- */
/* ====================================================================================== */
/* ==== Memory ==== */
void EvrRtxMemoryInit (void *mem, uint32_t size, uint32_t result) {
	// trec_rec3(TREC_USER_OS, TREC_RTX5_EVT_MEMORYINIT, (uint32_t)mem, size, result);
}

void EvrRtxMemoryAlloc (void *mem, uint32_t size, uint32_t type, void *block) {
	// trec_rec4(TREC_USER_OS, TREC_RTX5_EVT_MEMORYALLOC, (uint32_t)mem, size, type, (uint32_t)block);
}

void EvrRtxMemoryFree (void *mem, void *block, uint32_t result) {
	// trec_rec3(TREC_USER_OS, TREC_RTX5_EVT_MEMORYFREE, (uint32_t)mem, (uint32_t)block, result);
}

void EvrRtxMemoryBlockInit (osRtxMpInfo_t *mp_info, uint32_t block_count, uint32_t block_size, void *block_mem) {
	// trec_rec4(TREC_USER_OS, TREC_RTX5_EVT_MEMORYBLOCKINIT, (uint32_t)mp_info, block_count, block_size, (uint32_t)block_mem);
}

void EvrRtxMemoryBlockAlloc (osRtxMpInfo_t *mp_info, void *block) {
	// trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_MEMORYBLOCKALLOC, (uint32_t)mp_info, (uint32_t)block);
}

void EvrRtxMemoryBlockFree (osRtxMpInfo_t *mp_info, void *block, int32_t status) {
	// trec_rec3(TREC_USER_OS, TREC_RTX5_EVT_MEMORYBLOCKFREE, (uint32_t)mp_info, (uint32_t)block, status);
}

/* ====================================================================================== */
/* ==== Kernel ==== */
void EvrRtxKernelError (int32_t status) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_KERNELERROR, status);
}

void EvrRtxKernelInitialize (void) {
	trec_rec0(TREC_USER_OS, TREC_RTX5_EVT_KERNELINITIALIZE);
}

void EvrRtxKernelInitialized (void) {
	trec_rec0(TREC_USER_OS, TREC_RTX5_EVT_KERNELINITIALIZED);
}

void EvrRtxKernelGetInfo (osVersion_t *version, char *id_buf, uint32_t id_size) {
	// trec_rec3(TREC_USER_OS, TREC_RTX5_EVT_KERNELGETINFO, version->kernel, (uint32_t)id_buf, id_size);
}

void EvrRtxKernelInfoRetrieved (const osVersion_t *version, const char *id_buf, uint32_t id_size) {
	// trec_rec3(TREC_USER_OS, TREC_RTX5_EVT_KERNELINFORETRIEVED, version->kernel, (uint32_t)id_buf, id_size);
}

void EvrRtxKernelGetState (osKernelState_t state) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_KERNELGETSTATE, state);
}

void EvrRtxKernelStart (void) {
	trec_rec0(TREC_USER_OS, TREC_RTX5_EVT_KERNELSTART);
}

void EvrRtxKernelStarted (void) {
	trec_rec0(TREC_USER_OS, TREC_RTX5_EVT_KERNELSTARTED);
}

void EvrRtxKernelLock (void) {
	trec_rec0(TREC_USER_OS, TREC_RTX5_EVT_KERNELLOCK);
}

void EvrRtxKernelLocked (int32_t lock) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_KERNELLOCKED, lock);
}

void EvrRtxKernelUnlock (void) {
	trec_rec0(TREC_USER_OS, TREC_RTX5_EVT_KERNELUNLOCK);
}

void EvrRtxKernelUnlocked (int32_t lock) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_KERNELUNLOCKED, lock);
}

void EvrRtxKernelRestoreLock (int32_t lock) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_KERNELRESTORELOCK, lock);
}

void EvrRtxKernelLockRestored (int32_t lock) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_KERNELLOCKRESTORED, lock);
}

void EvrRtxKernelSuspend (void) {
	trec_rec0(TREC_USER_OS, TREC_RTX5_EVT_KERNELSUSPEND);
}

void EvrRtxKernelSuspended (uint32_t sleep_ticks) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_KERNELSUSPENDED, sleep_ticks);
}

void EvrRtxKernelResume (uint32_t sleep_ticks) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_KERNELRESUME, sleep_ticks);
}

void EvrRtxKernelResumed (void) {
	trec_rec0(TREC_USER_OS, TREC_RTX5_EVT_KERNELRESUMED);
}

void EvrRtxKernelGetTickCount (uint32_t count) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_KERNELGETTICKCOUNT, count);
}

void EvrRtxKernelGetTickFreq (uint32_t freq) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_KERNELGETTICKFREQ, freq);
}

void EvrRtxKernelGetSysTimerCount (uint32_t count) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_KERNELGETSYSTIMERCOUNT, count);
}

void EvrRtxKernelGetSysTimerFreq (uint32_t freq) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_KERNELGETSYSTIMERFREQ, freq);
}


/* ====================================================================================== */
/* ==== Thread ==== */
void EvrRtxThreadError (osThreadId_t thread_id, int32_t status) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_THREADERROR, (uint32_t)thread_id, status);
}

void EvrRtxThreadNew (osThreadFunc_t func, void *argument, const osThreadAttr_t *attr) {

}

void EvrRtxThreadCreated (osThreadId_t thread_id, uint32_t thread_addr, const char *name) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_THREADCREATED, (uint32_t)thread_id, thread_addr);
	trec_recMsg(TREC_USER_OS, TREC_RTX5_EVT_THREADCREATED, name);
}

void EvrRtxThreadGetName (osThreadId_t thread_id, const char *name) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_THREADGETNAME, (uint32_t)thread_id);
	trec_recMsg(TREC_USER_OS, TREC_RTX5_EVT_THREADGETNAME, name);
}

void EvrRtxThreadGetId (osThreadId_t thread_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_THREADGETID, (uint32_t)thread_id);
}

void EvrRtxThreadGetState (osThreadId_t thread_id, osThreadState_t state) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_THREADGETSTATE, (uint32_t)thread_id, state);
}

void EvrRtxThreadGetStackSize (osThreadId_t thread_id, uint32_t stack_size) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_THREADGETSTACKSIZE, (uint32_t)thread_id, stack_size);
}

void EvrRtxThreadGetStackSpace (osThreadId_t thread_id, uint32_t stack_space) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_THREADGETSTACKSPACE, (uint32_t)thread_id, stack_space);
}

void EvrRtxThreadSetPriority (osThreadId_t thread_id, osPriority_t priority) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_THREADSETPRIORITY, (uint32_t)thread_id, priority);
}

void EvrRtxThreadPriorityUpdated (osThreadId_t thread_id, osPriority_t priority) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_THREADPRIORITYUPDATED, (uint32_t)thread_id, priority);
}

void EvrRtxThreadGetPriority (osThreadId_t thread_id, osPriority_t priority) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_THREADGETPRIORITY, (uint32_t)thread_id, priority);
}

void EvrRtxThreadYield (void) {
	trec_rec0(TREC_USER_OS, TREC_RTX5_EVT_THREADYIELD);
}

void EvrRtxThreadSuspend (osThreadId_t thread_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_THREADSUSPEND, (uint32_t)thread_id);
}

void EvrRtxThreadSuspended (osThreadId_t thread_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_THREADSUSPENDED, (uint32_t)thread_id);
}

void EvrRtxThreadResume (osThreadId_t thread_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_THREADRESUME, (uint32_t)thread_id);
}

void EvrRtxThreadResumed (osThreadId_t thread_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_THREADRESUMED, (uint32_t)thread_id);
}

void EvrRtxThreadDetach (osThreadId_t thread_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_THREADDETACH, (uint32_t)thread_id);
}

void EvrRtxThreadDetached (osThreadId_t thread_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_THREADDETACHED, (uint32_t)thread_id);
}

void EvrRtxThreadJoin (osThreadId_t thread_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_THREADJOIN, (uint32_t)thread_id);
}

void EvrRtxThreadJoinPending (osThreadId_t thread_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_THREADJOINPENDING, (uint32_t)thread_id);
}

void EvrRtxThreadJoined (osThreadId_t thread_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_THREADJOINED, (uint32_t)thread_id);
}

void EvrRtxThreadBlocked (osThreadId_t thread_id, uint32_t timeout) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_THREADBLOCKED, (uint32_t)thread_id, timeout);
}

void EvrRtxThreadUnblocked (osThreadId_t thread_id, uint32_t ret_val) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_THREADUNBLOCKED, (uint32_t)thread_id, ret_val);
}

void EvrRtxThreadPreempted (osThreadId_t thread_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_THREADPREEMPTED, (uint32_t)thread_id);
}

void EvrRtxThreadSwitched (osThreadId_t thread_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_THREADSWITCHED, (uint32_t)thread_id);
}

void EvrRtxThreadExit (void) {
	trec_rec0(TREC_USER_OS, TREC_RTX5_EVT_THREADEXIT);
}

void EvrRtxThreadTerminate (osThreadId_t thread_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_THREADTERMINATE, (uint32_t)thread_id);
}

void EvrRtxThreadDestroyed (osThreadId_t thread_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_THREADDESTROYED, (uint32_t)thread_id);
}

void EvrRtxThreadGetCount (uint32_t count) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_THREADGETCOUNT, count);
}

void EvrRtxThreadEnumerate (osThreadId_t *thread_array, uint32_t array_items, uint32_t count) {

}


/* ====================================================================================== */
/* ==== Thread flags ==== */
void EvrRtxThreadFlagsError (osThreadId_t thread_id, int32_t status) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_THREADFLAGSERROR, (uint32_t)thread_id, status);
}

void EvrRtxThreadFlagsSet (osThreadId_t thread_id, uint32_t flags) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_THREADFLAGSSET, (uint32_t)thread_id, flags);
}

void EvrRtxThreadFlagsSetDone (osThreadId_t thread_id, uint32_t thread_flags) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_THREADFLAGSSETDONE, (uint32_t)thread_id, thread_flags);
}

void EvrRtxThreadFlagsClear (uint32_t flags) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_THREADFLAGSCLEAR, flags);
}

void EvrRtxThreadFlagsClearDone (uint32_t thread_flags) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_THREADFLAGSCLEARDONE, thread_flags);
}

void EvrRtxThreadFlagsGet (uint32_t thread_flags) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_THREADFLAGSGET, thread_flags);
}

void EvrRtxThreadFlagsWait (uint32_t flags, uint32_t options, uint32_t timeout) {
	trec_rec3(TREC_USER_OS, TREC_RTX5_EVT_THREADFLAGSWAIT, flags, options, timeout);
}

void EvrRtxThreadFlagsWaitPending (uint32_t flags, uint32_t options, uint32_t timeout) {
	trec_rec3(TREC_USER_OS, TREC_RTX5_EVT_THREADFLAGSWAITPENDING, flags, options, timeout);
}

void EvrRtxThreadFlagsWaitTimeout (osThreadId_t thread_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_THREADFLAGSWAITTIMEOUT, (uint32_t)thread_id);
}

void EvrRtxThreadFlagsWaitCompleted (uint32_t flags, uint32_t options, uint32_t thread_flags, osThreadId_t thread_id) {
	trec_rec4(TREC_USER_OS, TREC_RTX5_EVT_THREADFLAGSWAITCOMPLETED, flags, options, thread_flags, (uint32_t)thread_id);
}

void EvrRtxThreadFlagsWaitNotCompleted (uint32_t flags, uint32_t options) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_THREADFLAGSWAITNOTCOMPLETED, flags, options);
}


/* ====================================================================================== */
/* ==== Wait ==== */
void EvrRtxDelayError (int32_t status) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_DELAYERROR, status);
}

void EvrRtxDelay (uint32_t ticks) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_DELAY, ticks);
}

void EvrRtxDelayUntil (uint32_t ticks) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_DELAYUNTIL, ticks);
}

void EvrRtxDelayStarted (uint32_t ticks) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_DELAYSTARTED, ticks);
}

void EvrRtxDelayUntilStarted (uint32_t ticks) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_DELAYUNTILSTARTED, ticks);
}

void EvrRtxDelayCompleted (osThreadId_t thread_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_DELAYCOMPLETED, (uint32_t)thread_id);
}


/* ====================================================================================== */
/* ==== Timer ==== */
void EvrRtxTimerError (osTimerId_t timer_id, int32_t status) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_TIMERERROR, (uint32_t)timer_id, status);
}

void EvrRtxTimerCallback (osTimerFunc_t func, void *argument) {
	// trec_rec(TREC_USER_OS, TREC_RTX5_EVT_TIMERCALLBACK, (uint32_t)timer_id);
}

void EvrRtxTimerNew (osTimerFunc_t func, osTimerType_t type, void *argument, const osTimerAttr_t *attr) {
	// trec_rec(TREC_USER_OS, TREC_RTX5_EVT_TIMERNEW, (uint32_t)timer_id);
}

void EvrRtxTimerCreated (osTimerId_t timer_id, const char *name) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_TIMERCREATED, (uint32_t)timer_id);
	trec_recMsg(TREC_USER_OS, TREC_RTX5_EVT_TIMERCREATED, name);
}

void EvrRtxTimerGetName (osTimerId_t timer_id, const char *name) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_TIMERGETNAME, (uint32_t)timer_id);
	trec_recMsg(TREC_USER_OS, TREC_RTX5_EVT_TIMERGETNAME, name);
}

void EvrRtxTimerStart (osTimerId_t timer_id, uint32_t ticks) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_TIMERSTART, (uint32_t)timer_id, ticks);
}

void EvrRtxTimerStarted (osTimerId_t timer_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_TIMERSTARTED, (uint32_t)timer_id);
}

void EvrRtxTimerStop (osTimerId_t timer_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_TIMERSTOP, (uint32_t)timer_id);
}

void EvrRtxTimerStopped (osTimerId_t timer_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_TIMERSTOPPED, (uint32_t)timer_id);
}

void EvrRtxTimerIsRunning (osTimerId_t timer_id, uint32_t running) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_TIMERISRUNNING, (uint32_t)timer_id, running);
}

void EvrRtxTimerDelete (osTimerId_t timer_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_TIMERDELETE, (uint32_t)timer_id);
}

void EvrRtxTimerDestroyed (osTimerId_t timer_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_TIMERDESTROYED, (uint32_t)timer_id);
}



/* ====================================================================================== */
/* ==== Event flags ==== */
void EvrRtxEventFlagsError (osEventFlagsId_t ef_id, int32_t status) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_EVENTFLAGSERROR, (uint32_t)ef_id, status);
}

void EvrRtxEventFlagsNew (const osEventFlagsAttr_t *attr) {
	// trec_recMem(TREC_USER_OS, TREC_RTX5_EVT_EVENTFLAGSNEW, );
}

void EvrRtxEventFlagsCreated (osEventFlagsId_t ef_id, const char *name) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_EVENTFLAGSCREATED, (uint32_t)ef_id);
	trec_recMsg(TREC_USER_OS, TREC_RTX5_EVT_EVENTFLAGSCREATED, name);
}

void EvrRtxEventFlagsGetName (osEventFlagsId_t ef_id, const char *name) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_EVENTFLAGSGETNAME, (uint32_t)ef_id);
	trec_recMsg(TREC_USER_OS, TREC_RTX5_EVT_EVENTFLAGSGETNAME, name);
}

void EvrRtxEventFlagsSet (osEventFlagsId_t ef_id, uint32_t flags) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_EVENTFLAGSSET, (uint32_t)ef_id, flags);
}

void EvrRtxEventFlagsSetDone (osEventFlagsId_t ef_id, uint32_t event_flags) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_EVENTFLAGSSETDONE, (uint32_t)ef_id, event_flags);
}

void EvrRtxEventFlagsClear (osEventFlagsId_t ef_id, uint32_t flags) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_EVENTFLAGSCLEAR, (uint32_t)ef_id, flags);
}

void EvrRtxEventFlagsClearDone (osEventFlagsId_t ef_id, uint32_t event_flags) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_EVENTFLAGSCLEARDONE, (uint32_t)ef_id, event_flags);
}

void EvrRtxEventFlagsGet (osEventFlagsId_t ef_id, uint32_t event_flags) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_EVENTFLAGSGET, (uint32_t)ef_id, event_flags);
}

void EvrRtxEventFlagsWait (osEventFlagsId_t ef_id, uint32_t flags, uint32_t options, uint32_t timeout) {
	trec_rec4(TREC_USER_OS, TREC_RTX5_EVT_EVENTFLAGSWAIT, (uint32_t)ef_id, flags, options, timeout);
}

void EvrRtxEventFlagsWaitPending (osEventFlagsId_t ef_id, uint32_t flags, uint32_t options, uint32_t timeout) {
	trec_rec4(TREC_USER_OS, TREC_RTX5_EVT_EVENTFLAGSWAITPENDING, (uint32_t)ef_id, flags, options, timeout);
}

void EvrRtxEventFlagsWaitTimeout (osEventFlagsId_t ef_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_EVENTFLAGSWAITTIMEOUT, (uint32_t)ef_id);
}

void EvrRtxEventFlagsWaitCompleted (osEventFlagsId_t ef_id, uint32_t flags, uint32_t options, uint32_t event_flags) {
	trec_rec4(TREC_USER_OS, TREC_RTX5_EVT_EVENTFLAGSWAITCOMPLETED, (uint32_t)ef_id, flags, options, event_flags);
}

void EvrRtxEventFlagsWaitNotCompleted (osEventFlagsId_t ef_id, uint32_t flags, uint32_t options) {
	trec_rec3(TREC_USER_OS, TREC_RTX5_EVT_EVENTFLAGSWAITNOTCOMPLETED, (uint32_t)ef_id, flags, options);
}

void EvrRtxEventFlagsDelete (osEventFlagsId_t ef_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_EVENTFLAGSDELETE, (uint32_t)ef_id);
}

void EvrRtxEventFlagsDestroyed (osEventFlagsId_t ef_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_EVENTFLAGSDESTROYED, (uint32_t)ef_id);
}



/* ====================================================================================== */
/* ==== Mutex ==== */
void EvrRtxMutexError (osMutexId_t mutex_id, int32_t status) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_MUTEXERROR, (uint32_t)mutex_id, status);
}

void EvrRtxMutexNew (const osMutexAttr_t *attr) {
	// trec_rec(TREC_USER_OS, TREC_RTX5_EVT_MUTEXNEW, (uint32_t)mutex_id);
}

void EvrRtxMutexCreated (osMutexId_t mutex_id, const char *name) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_MUTEXCREATED, (uint32_t)mutex_id);
	trec_recMsg(TREC_USER_OS, TREC_RTX5_EVT_MUTEXCREATED, name);
}

void EvrRtxMutexGetName (osMutexId_t mutex_id, const char *name) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_MUTEXGETNAME, (uint32_t)mutex_id);
	trec_recMsg(TREC_USER_OS, TREC_RTX5_EVT_MUTEXGETNAME, name);
}

void EvrRtxMutexAcquire (osMutexId_t mutex_id, uint32_t timeout) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_MUTEXACQUIRE, (uint32_t)mutex_id, timeout);
}

void EvrRtxMutexAcquirePending (osMutexId_t mutex_id, uint32_t timeout) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_MUTEXACQUIREPENDING, (uint32_t)mutex_id, timeout);
}

void EvrRtxMutexAcquireTimeout (osMutexId_t mutex_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_MUTEXACQUIRETIMEOUT, (uint32_t)mutex_id);
}

void EvrRtxMutexAcquired (osMutexId_t mutex_id, uint32_t lock) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_MUTEXACQUIRED, (uint32_t)mutex_id, lock);
}

void EvrRtxMutexNotAcquired (osMutexId_t mutex_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_MUTEXNOTACQUIRED, (uint32_t)mutex_id);
}

void EvrRtxMutexRelease (osMutexId_t mutex_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_MUTEXRELEASE, (uint32_t)mutex_id);
}

void EvrRtxMutexReleased (osMutexId_t mutex_id, uint32_t lock) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_MUTEXRELEASED, (uint32_t)mutex_id, lock);
}

void EvrRtxMutexGetOwner (osMutexId_t mutex_id, osThreadId_t thread_id) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_MUTEXGETOWNER, (uint32_t)mutex_id, (uint32_t)thread_id);
}

void EvrRtxMutexDelete (osMutexId_t mutex_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_MUTEXDELETE, (uint32_t)mutex_id);
}

void EvrRtxMutexDestroyed (osMutexId_t mutex_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_MUTEXDESTROYED, (uint32_t)mutex_id);
}



/* ====================================================================================== */
/* ==== Semaphore ==== */
void EvrRtxSemaphoreError (osSemaphoreId_t semaphore_id, int32_t status) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_SEMAPHOREERROR, (uint32_t)semaphore_id, status);
}

void EvrRtxSemaphoreNew (uint32_t max_count, uint32_t initial_count, const osSemaphoreAttr_t *attr) {
	// trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_SEMAPHORENEW, max_count, initial_count);
}

void EvrRtxSemaphoreCreated (osSemaphoreId_t semaphore_id, const char *name) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_SEMAPHORECREATED, (uint32_t)semaphore_id);
	trec_recMsg(TREC_USER_OS, TREC_RTX5_EVT_SEMAPHORECREATED, name);
}

void EvrRtxSemaphoreGetName (osSemaphoreId_t semaphore_id, const char *name) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_SEMAPHOREGETNAME, (uint32_t)semaphore_id);
	trec_recMsg(TREC_USER_OS, TREC_RTX5_EVT_SEMAPHOREGETNAME, name);
}

void EvrRtxSemaphoreAcquire (osSemaphoreId_t semaphore_id, uint32_t timeout) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_SEMAPHOREACQUIRE, (uint32_t)semaphore_id, timeout);
}

void EvrRtxSemaphoreAcquirePending (osSemaphoreId_t semaphore_id, uint32_t timeout) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_SEMAPHOREACQUIREPENDING, (uint32_t)semaphore_id, timeout);
}

void EvrRtxSemaphoreAcquireTimeout (osSemaphoreId_t semaphore_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_SEMAPHOREACQUIRETIMEOUT, (uint32_t)semaphore_id);
}

void EvrRtxSemaphoreAcquired (osSemaphoreId_t semaphore_id, uint32_t tokens) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_SEMAPHOREACQUIRED, (uint32_t)semaphore_id, tokens);
}

void EvrRtxSemaphoreNotAcquired (osSemaphoreId_t semaphore_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_SEMAPHORENOTACQUIRED, (uint32_t)semaphore_id);
}

void EvrRtxSemaphoreRelease (osSemaphoreId_t semaphore_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_SEMAPHORERELEASE, (uint32_t)semaphore_id);
}

void EvrRtxSemaphoreReleased (osSemaphoreId_t semaphore_id, uint32_t tokens) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_SEMAPHORERELEASED, (uint32_t)semaphore_id, tokens);
}

void EvrRtxSemaphoreGetCount (osSemaphoreId_t semaphore_id, uint32_t count) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_SEMAPHOREGETCOUNT, (uint32_t)semaphore_id, count);
}

void EvrRtxSemaphoreDelete (osSemaphoreId_t semaphore_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_SEMAPHOREDELETE, (uint32_t)semaphore_id);
}

void EvrRtxSemaphoreDestroyed (osSemaphoreId_t semaphore_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_SEMAPHOREDESTROYED, (uint32_t)semaphore_id);
}



/* ====================================================================================== */
/* ==== Memory pool ==== */
void EvrRtxMemoryPoolError (osMemoryPoolId_t mp_id, int32_t status) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_MEMORYPOOLERROR, (uint32_t)mp_id, status);
}

void EvrRtxMemoryPoolNew (uint32_t block_count, uint32_t block_size, const osMemoryPoolAttr_t *attr) {
	// trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_MEMORYPOOLNEW, block_count, block_size);
}

void EvrRtxMemoryPoolCreated (osMemoryPoolId_t mp_id, const char *name) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_MEMORYPOOLCREATED, (uint32_t)mp_id);
	trec_recMsg(TREC_USER_OS, TREC_RTX5_EVT_MEMORYPOOLCREATED, name);
}

void EvrRtxMemoryPoolGetName (osMemoryPoolId_t mp_id, const char *name) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_MEMORYPOOLGETNAME, (uint32_t)mp_id);
	trec_recMsg(TREC_USER_OS, TREC_RTX5_EVT_MEMORYPOOLGETNAME, name);
}

void EvrRtxMemoryPoolAlloc (osMemoryPoolId_t mp_id, uint32_t timeout) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_MEMORYPOOLALLOC, (uint32_t)mp_id, timeout);
}

void EvrRtxMemoryPoolAllocPending (osMemoryPoolId_t mp_id, uint32_t timeout) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_MEMORYPOOLALLOCPENDING, (uint32_t)mp_id, timeout);
}

void EvrRtxMemoryPoolAllocTimeout (osMemoryPoolId_t mp_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_MEMORYPOOLALLOCTIMEOUT, (uint32_t)mp_id);
}

void EvrRtxMemoryPoolAllocated (osMemoryPoolId_t mp_id, void *block) {
	// trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_MEMORYPOOLALLOCATED, (uint32_t)mp_id);
}

void EvrRtxMemoryPoolAllocFailed (osMemoryPoolId_t mp_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_MEMORYPOOLALLOCFAILED, (uint32_t)mp_id);
}

void EvrRtxMemoryPoolFree (osMemoryPoolId_t mp_id, void *block) {
	// trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_MEMORYPOOLFREE, (uint32_t)mp_id);
}

void EvrRtxMemoryPoolDeallocated (osMemoryPoolId_t mp_id, void *block) {
	// trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_MEMORYPOOLDEALLOCATED, (uint32_t)mp_id);
}

void EvrRtxMemoryPoolFreeFailed (osMemoryPoolId_t mp_id, void *block) {
	// trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_MEMORYPOOLFREEFAILED, (uint32_t)mp_id);
}

void EvrRtxMemoryPoolGetCapacity (osMemoryPoolId_t mp_id, uint32_t capacity) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_MEMORYPOOLGETCAPACITY, (uint32_t)mp_id, capacity);
}

void EvrRtxMemoryPoolGetBlockSize (osMemoryPoolId_t mp_id, uint32_t block_size) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_MEMORYPOOLGETBLOCKSIZE, (uint32_t)mp_id, block_size);
}

void EvrRtxMemoryPoolGetCount (osMemoryPoolId_t mp_id, uint32_t count) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_MEMORYPOOLGETCOUNT, (uint32_t)mp_id, count);
}

void EvrRtxMemoryPoolGetSpace (osMemoryPoolId_t mp_id, uint32_t space) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_MEMORYPOOLGETSPACE, (uint32_t)mp_id, space);
}

void EvrRtxMemoryPoolDelete (osMemoryPoolId_t mp_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_MEMORYPOOLDELETE, (uint32_t)mp_id);
}

void EvrRtxMemoryPoolDestroyed (osMemoryPoolId_t mp_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_MEMORYPOOLDESTROYED, (uint32_t)mp_id);
}


/* ====================================================================================== */
/* ==== Message queue ==== */
void EvrRtxMessageQueueError (osMessageQueueId_t mq_id, int32_t status) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_MESSAGEQUEUEERROR, (uint32_t)mq_id, status);
}

void EvrRtxMessageQueueNew (uint32_t msg_count, uint32_t msg_size, const osMessageQueueAttr_t *attr) {
	// trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_MESSAGEQUEUENEW, msg_count, msg_size);
}

void EvrRtxMessageQueueCreated (osMessageQueueId_t mq_id, const char *name) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_MESSAGEQUEUECREATED, (uint32_t)mq_id);
	trec_recMsg(TREC_USER_OS, TREC_RTX5_EVT_MESSAGEQUEUECREATED, name);
}

void EvrRtxMessageQueueGetName (osMessageQueueId_t mq_id, const char *name) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_MESSAGEQUEUEGETNAME, (uint32_t)mq_id);
	trec_recMsg(TREC_USER_OS, TREC_RTX5_EVT_MESSAGEQUEUEGETNAME, name);
}

void EvrRtxMessageQueuePut (osMessageQueueId_t mq_id, const void *msg_ptr, uint8_t msg_prio, uint32_t timeout) {
	// trec_rec3(TREC_USER_OS, TREC_RTX5_EVT_MESSAGEQUEUEPUT, (uint32_t)mq_id, msg_prio, timeout);
}

void EvrRtxMessageQueuePutPending (osMessageQueueId_t mq_id, const void *msg_ptr, uint32_t timeout) {
	// trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_MESSAGEQUEUEPUTPENDING, (uint32_t)mq_id, timeout);
}

void EvrRtxMessageQueuePutTimeout (osMessageQueueId_t mq_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_MESSAGEQUEUEPUTTIMEOUT, (uint32_t)mq_id);
}

void EvrRtxMessageQueueInsertPending (osMessageQueueId_t mq_id, const void *msg_ptr) {
	// trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_MESSAGEQUEUEINSERTPENDING, (uint32_t)mq_id);
}

void EvrRtxMessageQueueInserted (osMessageQueueId_t mq_id, const void *msg_ptr) {
	// trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_MESSAGEQUEUEINSERTED, (uint32_t)mq_id);
}

void EvrRtxMessageQueueNotInserted (osMessageQueueId_t mq_id, const void *msg_ptr) {
	// trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_MESSAGEQUEUENOTINSERTED, (uint32_t)mq_id);
}

void EvrRtxMessageQueueGet (osMessageQueueId_t mq_id, void *msg_ptr, uint8_t *msg_prio, uint32_t timeout) {
	// trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_MESSAGEQUEUEGET, (uint32_t)mq_id, timeout);
}

void EvrRtxMessageQueueGetPending (osMessageQueueId_t mq_id, void *msg_ptr, uint32_t timeout) {
	// trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_MESSAGEQUEUEGETPENDING, (uint32_t)mq_id, timeout);
}

void EvrRtxMessageQueueGetTimeout (osMessageQueueId_t mq_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_MESSAGEQUEUEGETTIMEOUT, (uint32_t)mq_id);
}

void EvrRtxMessageQueueRetrieved (osMessageQueueId_t mq_id, void *msg_ptr) {
	// trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_MESSAGEQUEUERETRIEVED, (uint32_t)mq_id);
}

void EvrRtxMessageQueueNotRetrieved (osMessageQueueId_t mq_id, void *msg_ptr) {
	// trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_MESSAGEQUEUENOTRETRIEVED, (uint32_t)mq_id);
}

void EvrRtxMessageQueueGetCapacity (osMessageQueueId_t mq_id, uint32_t capacity) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_MESSAGEQUEUEGETCAPACITY, (uint32_t)mq_id, capacity);
}

void EvrRtxMessageQueueGetMsgSize (osMessageQueueId_t mq_id, uint32_t msg_size) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_MESSAGEQUEUEGETMSGSIZE, (uint32_t)mq_id, msg_size);
}

void EvrRtxMessageQueueGetCount (osMessageQueueId_t mq_id, uint32_t count) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_MESSAGEQUEUEGETCOUNT, (uint32_t)mq_id, count);
}

void EvrRtxMessageQueueGetSpace (osMessageQueueId_t mq_id, uint32_t space) {
	trec_rec2(TREC_USER_OS, TREC_RTX5_EVT_MESSAGEQUEUEGETSPACE, (uint32_t)mq_id, space);
}

void EvrRtxMessageQueueReset (osMessageQueueId_t mq_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_MESSAGEQUEUERESET, (uint32_t)mq_id);
}

void EvrRtxMessageQueueResetDone (osMessageQueueId_t mq_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_MESSAGEQUEUERESETDONE, (uint32_t)mq_id);
}

void EvrRtxMessageQueueDelete (osMessageQueueId_t mq_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_MESSAGEQUEUEDELETE, (uint32_t)mq_id);
}

void EvrRtxMessageQueueDestroyed (osMessageQueueId_t mq_id) {
	trec_rec1(TREC_USER_OS, TREC_RTX5_EVT_MESSAGEQUEUEDESTROYED, (uint32_t)mq_id);
}

#pragma GCC diagnostic warning "-Wunused-function"		/* Restore the warnings from unused private function in your lib */
#pragma GCC diagnostic warning "-Wunused-parameter"
