
/* RTX needs this file to exist in include path */

#ifndef RTE_COMPONENTS_H
#define RTE_COMPONENTS_H

#define CMSIS_device_header "hardware.h"

/* this file requires tlib and enables rtx event recording through tbug streams */
#include <trec_rtx5.h>

#endif /* RTE_COMPONENTS_H */
